import React, { Component, Fragment } from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline'
import NavBar from '../NavBar';
import Paper from '../Paper';
import BlogContext from '../../utils/context';
import Snackbar from '@material-ui/core/Snackbar';
import { getPrivilege } from '../Privilege';

class Layout extends Component {
    constructor (props) {
        super(props);
        this.state = {
            hasError: false,
        };
    }

    static getDerivedStateFromError(error) {
        return {
            hasError: true
        };
    }

    componentDidCatch(error, info) {
        console.log(error, info);
    }

    componentDidMount () {
        const { ctx } = this.props;
        const cb = ({ name, login, role }) => {
            ctx.setLogInfo({ name, login, role})
        }
        if (!ctx.user.login) {
            getPrivilege(cb);
        }
    }

    render () {
        if (this.state.hasError) {
            return <h4>Something went wrong</h4>
        }
        return (
            <Fragment>
                <BlogContext.Consumer>
                    {ctx => {
                        return ctx.snack.open && (
                            <Snackbar
                                anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                                key="top,center"
                                open={ctx.snack.open}
                                onClose={ctx.toggleSnack.bind(this, { show: false, msg: ''})}
                                ContentProps={{
                                    'aria-describedby': 'message-id',
                                }}
                                message={<span id="message-id">{ctx.snack.msg}</span>} />
                        )}}
                </BlogContext.Consumer>
                <CssBaseline />
                <Container maxWidth="md">
                    <BlogContext.Consumer>
                        {
                            ctx => <NavBar {...ctx.user}/>
                        }
                    </BlogContext.Consumer>
                    <BlogContext.Consumer>
                        { ctx => ctx.UsePaper && <Paper /> }
                    </BlogContext.Consumer>
                    <div className="blog-container">
                        {this.props.children}
                    </div>
                </Container>
            </Fragment>
        )
    }
}

export default Layout;
