import React, { useContext } from 'react';
import BlogContext from '../../utils/context';
import API from '../../utils/api';
import wechatCode from '../../static/wechat_code.jpeg'
import {
    Route
  } from "react-router-dom";

export async function getPrivilege (cb) {
    const result = await API.privilege();
    const { code, data } = result || {};
    if (code === 200112) {
        const { username, role } = data || {};
        cb({ login: true, name: username || '---', role })
    }
}

function NoPrivilege () {
    return (
    <div>
        <h3>您没有权限创建文章，请联系管理员</h3>
        <h3>邮箱：519015591@qq.com</h3>
        <span style={{verticalAlign: 'top', fontSize: 17, fontWeight: 800 }}>
            微信： <img width="200" src={wechatCode} alt="Logo" />
        </span>
        
    </div>)
}

function Privilege({ component: Component, ...rest }) {

    const ctx = useContext(BlogContext);
    return (
        <Route
            {...rest}
            render={
                props => ctx.user.role === 'ADMIN'
                    ? <Component {...props}/>
                    : <NoPrivilege />
            }
        >
        </Route>
    )
}

export default Privilege;
