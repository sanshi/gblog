import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import useStyles from './index.style';

function ListPaper() {
    const classes = useStyles();
    return <Paper className={classes.mainFeaturedPost}>
              <img
                  style={{ display: 'none' }}
                  src="http://meisudci.oss-cn-beijing.aliyuncs.com/huge/MSBQ13715300131331.jpg"
                  alt="background"/>
              <div className={classes.overlay} />
              <Grid container>
                <Grid item md={6}>
                  <div className={classes.mainFeaturedPostContent}>
                    <Typography variant="h5" color="inherit" paragraph>
                    Explore The Unknown
                    </Typography>
                  </div>
                </Grid>
              </Grid>
          </Paper>
}

export default ListPaper;
