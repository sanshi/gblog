import React, { Fragment } from "react";
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import useStyles from './index.style';
import API from "../../utils/api";

const catalague = [
    {
        label: '全部',
        value: '#/list'
    },
    {
        label: '技术',
        value: '#/list/tech'
    },
    {
        label: '更新历史',
        value: '#/history'
    },
    {
        label: 'TODOLIST',
        value: '#/todo'
    }
]

function NavBar (props) {
    let sections = [...catalague ];

    if (props.role === 'ADMIN') {
        sections = [
            ...catalague,
            {
                label: '个人',
                value: '#/list/self'
            }
        ]
    }


    function handleClickOut () {
        API.logout().then(res => {
            if (res.code === 200113) {
                window.location.reload();
                return null;
            }
        });
    }

    const classes = useStyles();
    return (
        <Fragment>
            <Toolbar className={classes.toolbar}>
                <Typography
                    component="h3"
                    variant="h5"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.toolbarTitle}>
                    欢迎 {props.name}
                </Typography>
                {
                    props.login
                    ? 
                        <Button variant="outlined" size="small" onClick={handleClickOut}>
                            登出
                        </Button>
                    : 
                    <Button variant="outlined" size="small"  href="/#/sign/login">
                        登录
                    </Button>
                }
            </Toolbar>
            <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
                {sections.map(section => (
                    <Link
                        color="inherit"
                        noWrap
                        key={section.value}
                        variant="body2"
                        href={section.value}
                        className={classes.toolbarLink}>
                        {section.label}
                    </Link>
                ))}
            </Toolbar>
        </Fragment>
    )
}

export default NavBar;

