import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    // '@global': {
    //     strong: {
    //         fontWeight: theme.typography.fontWeightMedium
    //     }
    // },
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`
    },
    toolbarTitle: {
        flex: 1
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    }
}))

export default useStyles;