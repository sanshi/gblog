import React, { Suspense, Component } from 'react';
import { HashRouter as Router } from "react-router-dom";
import { routes, RouteWithSubRoutes } from './router';
import Layout from './components/Layout';
import BlogContext, { ctx } from './utils/context';

class App extends Component {

  constructor(props) {
    super(props);

    this.togglePaper = paper => {
      this.setState(prevState => {
        return {
          ...prevState.ctx,
          UsePaper: paper
        }
      })
    }

    this.toggleSnack = ({ show, msg }) => {
      this.setState(prevState => {
        return {
          ...prevState.ctx,
          snack: {
            open: show,
            msg: msg
          }
        }
      })
    }

    this.setLogInfo = ({ name, login, role = '' }) => {
      this.setState(prevState => {
        return {
          ...prevState.ctx,
          user: {
            name, login, role
          }
        }
      })
    }

    this.state = {
      ...ctx,
      togglePaper: this.togglePaper,
      toggleSnack: this.toggleSnack,
      setLogInfo: this.setLogInfo
    }
  }

  render () {
    return  (
      <Router>
        <BlogContext.Provider value={this.state}>
          <Suspense fallback={<div>Loading...</div>}>
            <Layout ctx={this.state}>
            {
              routes.map((route, idx) => {
                return  <RouteWithSubRoutes key={idx} {...route} />
              })
            }

            </Layout>
          </Suspense>
        </BlogContext.Provider>
      </Router>
    )
  }
}

export default App;
