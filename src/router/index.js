import React, { lazy } from 'react';
import { Route } from 'react-router-dom';
import Privilege from '../components/Privilege';

const BlogList = lazy(() => import(
  /* webpackChunkName: "blog-list" */
  '../pages/BlogList'
));
const BlogDetail = lazy(()=> import(
  /* webpackChunkName: "blog-detail" */
  '../pages/BlogDetail'
));

const BlogSign = lazy(() => import(
  /* webpackChunkName: "blog-sign" */
  '../pages/BlogLogin'
));

const BlogHistory = lazy(() => import(
  /* webpackChunkName: "blog-history" */
  '../pages/BlogHistory'
));

const BlogTodoList = lazy(() => import(
  /* webpackChunkName: "blog-todo" */
  '../pages/BlogTodoList'
));


const routes = [
  {
    path: '/',
    component: BlogList,
    exact: true
  },
    {
      path: '/list',
      component: BlogList,
      routes: [
        {
          path: '/:type',
          component: BlogList,
        },
      ]
    },
    {
      path: '/detail/:id',
      component: BlogDetail
    },
    {
      path: '/sign/:type',
      component: BlogSign
    },
    {
      path: '/history',
      component: BlogHistory
    },
    {
      path: '/todo',
      component: BlogTodoList
    }
];

const RouteWithSubRoutes = route => {
    return (
      <Route
        path={route.path}
        exact
        render={props => {
          // pass the sub-routes down to keep nesting
          if (route.needPrivilege) {
            return <Privilege component={route.component} {...props}/>
          }
          return <route.component {...props} routes={route.routes} />
        }}
      />
    );
  }

export {
    routes,
    RouteWithSubRoutes
}
