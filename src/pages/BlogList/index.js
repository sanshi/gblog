import React, { useState, useEffect, useContext } from "react";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import BlogItem from './item';
import api from '../../utils/api';
import useStyles from './index.style';
import BlogContext from "../../utils/context";

function BlogList () {
    const [lists, setLists] = useState([]);
    const ctx = useContext(BlogContext);
    const [pageConfig, setPage] = useState({
        page: 1,
        last: 1,
        total: 0
    })

    useEffect(() => {
        ctx.togglePaper(true);
    }, []);

    async function fetchList(page = 1) {
        const result = await api.getArticleList({ page, limit: 10 });
        const { code, data } = result;
        if (code === 200) {
            setLists(data.lists);
            setPage({ total: data.total, page, last: Math.ceil(data.total / 10) });
        }
    }

    const handlePrev = () => {
        if (pageConfig.page <= 1) {
            return
        }
        fetchList(pageConfig.page - 1 );
    }

    const handleNext = () => {
        if (pageConfig.page >= pageConfig.last) {
            return
        }
        fetchList(pageConfig.page + 1 );
    }

    useEffect(() => {
        fetchList();
    }, []);

    const classes = useStyles();
    if (!lists.length) {
        return null;
    }

    return (<Grid container spacing={2} className={classes.cardGrid}>
                {lists.map(list => (
                    <Grid item key={list.id} xs={12}>
                        <BlogItem {...list}/>
                    </Grid>
                ))}
                <div className={ classes.btnGroups }>
                    <Hidden smDown>
                        <span className={ classes.pagination }>
                            第 <span className={classes.page}>{pageConfig.page}</span> 页，
                            共 <span className={classes.page}>{pageConfig.last}</span> 页
                        </span>
                    </Hidden>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={ pageConfig.page === 1 }
                        className={ classes.btn }
                        onClick={ handlePrev }>
                        上一页
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={ pageConfig.page === pageConfig.last }
                        className={ classes.btn }
                        onClick={ handleNext }>
                        下一页
                    </Button>
                </div>
          </Grid>)
}

export default BlogList;
