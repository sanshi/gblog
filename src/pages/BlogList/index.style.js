import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  cardActionArea: {
    padding: theme.spacing(2),
    border: '1px solid #eeeeee',
    borderRadius: '6px',
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)"
  },
  title: {
    fontSize: '16px'
  },
  createdAt: {
    fontSize: '12px',
    color: '#666'
  },
  description: {
    fontSize: '16px'
  },
  btnGroups: {
    width: '100%',
    display: 'block',
    margin: 15,
    textAlign: 'center'
  },
  btn: {
    marginRight: theme.spacing(1)
  },
  pagination: {
    marginRight: 15
  },
  page: {
    color: '#3f51b5',
    fontSize: 18,
  }
}));
  
export default useStyles;
