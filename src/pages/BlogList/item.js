import React from 'react';
import CardActionArea from '@material-ui/core/CardActionArea';
import useStyles from './index.style';

function BlogItem(props) {
    const classes = useStyles();
    return(
        <CardActionArea
            className={classes.cardActionArea}
            component="a" href={`/#/detail/${props.id}`}>
            <div className={classes.title}>
                {props.title}
            </div>
            <div className={classes.createdAt}>
                {props.createdAt}
            </div>
            <div className={classes.description}>
                {props.description}
            </div>
            <div className={classes.datum}>
                {}
            </div>
        </CardActionArea>
    )
}

export default BlogItem;