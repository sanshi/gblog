import React, { useState, useEffect, useContext } from 'react';

import api from '../../utils/api';
import BlogContext from '../../utils/context';
import Markdown from '../../utils/md';

function BlogDetail(props) {
    const { id } = props.match.params || {};
    const [detail, setDetail] = useState({});
    const ctx = useContext(BlogContext);
    useEffect(() => {
        ctx.togglePaper(true);
    }, [])

    useEffect(() => {
        const fetchDetail = async () => {
            const result = await api.getArticleDetail({ id });
            const { code, data } = result;
            if (code === 200) {
                setDetail(data);
            }
        }
        fetchDetail();
    }, [id]);

    if(!detail.content) {
        return null;
    }
    const markdown = new Markdown()
    const content = markdown.render(detail.content)

    return <div dangerouslySetInnerHTML={{__html: content}}></div>
}

export default BlogDetail;
