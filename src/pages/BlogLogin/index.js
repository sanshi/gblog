import React, { useContext, useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import useStyles from './index.style'
import BlogContext from '../../utils/context';
import API from '../../utils/api';
import MESSAGE from '../../utils/msg';

function Register (props) {
  return (
    <Button
      fullWidth
      variant="contained"
      color="primary"
      onClick={props.onClick}>注册</Button>
  )
}

function Login(props) {
  return (
    <Button
      fullWidth
      variant="contained"
      color="primary"
      onClick={props.onClick}>
      登录
    </Button>
  )
}

function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built with love by the '}
      <Link color="inherit" href="https://material-ui.com/">
        Material-UI
      </Link>
      {' team.'}
    </Typography>
  );
}

export default function SignInSide(props) {
  console.log('props', props)
  const classes = useStyles();

  const ctx = useContext(BlogContext);
  const { type } = props.match.params || {};

  const [model, setModel] = useState({
    username: '',
    email: '',
    password: ''
  });

  const handleChange = name => event => {
    setModel({ ...model, [name]: event.target.value });
  };

  useEffect(() => {
    ctx.togglePaper(false);
  }, []);

  function showSnack(msg) {
    ctx.toggleSnack({ show: true, msg })
  }

  function hadLogin(username, role) {
    ctx.setLogInfo({ name: username, login: true, role });
    props.history.push('/list');
  }

  function handleClickLogin () {
    if (!model.username && !model.email) {
      showSnack('用户名和邮箱不能同时为空!');
      return 
    }

    if (!model.password) {
      showSnack('密码不能同时为空!');
      return
    }
    API.login({
      ...model
    }).then(res => {
      showSnack(MESSAGE[res.code] || res.msg)
      if (res.code === 200108) {
        // 登录成功
        const { username, role } = res.data || {};
        hadLogin(username, role)
      }
    })
  }

  function handleClickRegister () {
    if (!model.username && !model.email) {
      showSnack('用户名和邮箱不能同时为空!');
      return 
    }

    if (!model.password) {
      showSnack('密码不能同时为空!');
      return
    }
    API.register({
      ...model
    }).then(res => {
      showSnack(MESSAGE[res.code] || res.message);
      if (res.code === 200111) {
        showSnack('用户创建成功，请重新登录！');
        props.history.push('/sign/login')
      }
    })
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              { type === 'login' ? '登录' : '注册' }
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="姓名"
                name="username"
                autoComplete="username"
                onChange={handleChange('username')}
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="邮箱"
                name="email"
                autoComplete="email"
                onChange={handleChange('email')}
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="密码"
                type="password"
                id="password"
                onChange={handleChange('password')}
                autoComplete="current-password"
              />

              { type === 'login' ? <Login onClick={handleClickLogin}/> : <Register onClick={handleClickRegister}/>}
              <Grid container>
                <Grid item xs>
                </Grid>
                <Grid item>
                  <Link href="#/sign/register" variant="body2">
                    {"没有账户，去注册"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}>
                <MadeWithLove />
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
  );
}
