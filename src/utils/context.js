import { createContext } from 'react';

// 头部图片背景是否需要
export let ctx = {
    UsePaper: true,
    togglePaper: () => {},
    snack: {
        open: false,
        msg: '',
    },
    toggleSnack: () => {},
    user: {
        name: '',
        login: false,
        role: ''
    },
    setLogInfo: () => {}
};

const BlogContext = createContext(
    ctx
);

export default BlogContext;
