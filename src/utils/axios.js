import axios from 'axios';

axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    if (response.status === 200) {
      return Promise.resolve(response.data)
    }
    return Promise.resolve({
      code: -1,
      data: null
    })
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });

