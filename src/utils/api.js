// import {useEffect, useState, useReducer } from 'react';
import axios from 'axios';
const BASE_URL = '/api/v1blog';

const API =  {
    // 获取文章列表
    getArticleList(options) {
        return axios.get(`${BASE_URL}/article/list`, {
            params: {
                ...options
            }
        });
    },
    // 获取文章详情
    getArticleDetail(options) {
        return axios.get(`${BASE_URL}/article/detail/${options.id}`);
    },
    // 发布文章
    publishArticle(options) {
        return axios.post(`${BASE_URL}/article/create`, {
            ...options
        })
    },

    // 登录
    login(options) {
        return axios.post(`${BASE_URL}/user/login`, {
            ...options
        })
    },

    logout() {
        return axios.get(`${BASE_URL}/user/logout`)
    },

    // 注册
    register (options) {
        return axios.post(`${BASE_URL}/user/create`, {
            ...options
        })
    },

    // 获取用户权限
    privilege () {
        return axios.get(`${BASE_URL}/user/privilege`)
    }
}

// const dataFetchReducer = (state, action) => {
//     switch(action.type) {
//         case 'FETCH_INIT':
//             return {
//                 ...state,
//                 isLoading: true,
//                 isError: false,
//             };
//         case 'FETCH_SUCCESS':
//             return {
//                 ...state,
//                 isLoading: false,
//                 isError: false,
//                 data: action.payload,
//             };
//         case 'FETCH_FAILURE':
//             return {
//                 ...state,
//                 isLoading: false,
//                 isError: true,
//             };
//         default: 
//             throw new Error();
//     }
// }

// const useApi = (initialOptions = {}, initialData = {}) => {
//     const [options, setOptions] = useState(initialOptions);
//     const [state, dispatch] = useReducer(dataFetchReducer, {
//         isLoading: false,
//         isError: false,
//         data: initialData
//     })

//     useEffect(() => {
//         let didCancel = false;
//         const fetchData = async () => {
//             dispatch({ type: 'FETCH_INIT' })
//             try {
//                 const { rest, name } = options
//                 const result = await API[name]({rest});

//                 const {code, data} = result;
//                 if (code === 200 && data) {
//                     if (!didCancel) {
//                         dispatch({ type: 'FETCH_SUCCESS', payload: data })
//                     }
//                 }

//             } catch(error) {
//                 if (!didCancel) {
//                     dispatch({ type: 'FETCH_FAILURE' })
//                 }
//             }
//         }

//         fetchData();
//         return () => {
//             didCancel = true;
//         }
//     }, [options]);

//     const doFetch = opts => {
//         setOptions(opts);
//     }

//     return { ...state, doFetch };
// }


export default API;