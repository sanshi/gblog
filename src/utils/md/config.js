export default {
  preview: {
    fontSize: '14',
    fontFamily: 'Lato',
    codeBlockTheme: 'dracula',
    lineNumber: true,
    latexInlineOpen: '$',
    latexInlineClose: '$',
    latexBlockOpen: '$$',
    latexBlockClose: '$$',
    plantUMLServerAddress: 'http://www.plantuml.com/plantuml',
    scrollPastEnd: false,
    scrollSync: true,
    smartQuotes: true,
    breaks: true,
    smartArrows: false,
    allowCustomCSS: false,
    customCSS: '',
    sanitize: 'STRICT', // 'STRICT', 'ALLOW_STYLES', 'NONE'
    lineThroughCheckbox: true
  },
}