export const timeObj = () => {
  const tDate = new Date();
  return {
    year: tDate.getFullYear(),
    month: prefix(tDate.getMonth() + 1),
    day: prefix(tDate.getDate()),
    hour: prefix(tDate.getHours()),
    minute: prefix(tDate.getMinutes()),
    second: prefix(tDate.getSeconds()),
    ms: tDate.getSeconds(),
  }
}

export const prefix = num => {
  return num >= 0 && num < 10 ? `0${num}`: num;
}

export const dateStr = () => {
  const { year, month, day, hour, minute, second } = timeObj();
  return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}
